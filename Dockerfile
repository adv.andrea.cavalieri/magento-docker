FROM bitnami/magento:2.4.1-debian-10-r1
LABEL maintainer "Bitnami <containers@bitnami.com>"

## Install 'vim'
RUN install_packages vim

## Enable mod_ratelimit module
##RUN chgrp -R 0 /var/run && \
##    chmod -R g=u /var/run/

RUN whoami
RUN ls -lrt /var/run

##RUN chmod -R 777  /var/run/

## Modify the ports used by Apache by default
# It is also possible to change these environment variables at runtime
ENV APACHE_HTTP_PORT_NUMBER=8181
ENV APACHE_HTTPS_PORT_NUMBER=8143
EXPOSE 8181 8143
